// Підключення необхідних модулів
const { Builder, By } = require('selenium-webdriver');

// Створення об'єкта драйвера (в цьому прикладі використовується Chrome)
let driver = new Builder().forBrowser('chrome').build();

// Приклад тестового сценарію
async function simpleTest() {
    try {
        // Відкриття веб-сторінки
        await driver.get('https://www.example.com');
        
        // Отримання заголовка сторінки
        let title = await driver.getTitle();
        
        // Перевірка, чи вірно відображається заголовок сторінки
        if (title === 'Example Domain') {
            console.log('Тест пройшов успішно: заголовок сторінки вірний.');
        } else {
            console.error('Тест не вдалий: заголовок сторінки невірний.');
        }
    } catch (error) {
        console.error('Виникла помилка під час виконання тесту:', error);
    } finally {
        // Завершення сеансу браузера
        await driver.quit();
    }
}

// Виклик функції тесту
simpleTest();
